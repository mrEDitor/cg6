#include <cmath>
#include <cstring>
#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "Configuration.hpp"

const int DRAWING_QUADS = 0;
const int DRAWING_LINES = 1;
const int DRAWING_DISK = 2;
const int DRAWING_OCTAHEDRON = 3;
const int DRAWING_MOD = 4;
const char CONTROL_TIP[] = "<> to choose shape\n"
        "TGB to adjust color,\n"
        "V to switch texturing,\n"
        "N to switch culling,\n"
        "WASDQZ to move object,\n"
        "IJKL and mouse wheel to scale object,\n"
        "XC and mouse to rotate,\n"
        "space to reset all transformations";

std::vector<float> quads_vertices, lines_vertices, disk_vertices, octahedron_vertices;
std::vector<float> quads_textures, disk_textures, octahedron_textures;
std::vector<float> color;
bool texture_enabled, culling_enabled;
int window_width, window_height;
float straight_step, angle_step;
float nearest_plane, farest_plane;
float translate_x, translate_y, translate_z;
float scale_x, scale_y, scale_z;
float rotate_x, rotate_y, rotate_z;
int drawing_shape;
GLuint texture_handle;

int main(int argc, char *argv[]);
std::vector<float> build_disk(float radius, int slices);
std::vector<float> build_disk_texture(float radius, int slices);
void onKeyPress(const sf::Event &event);
void resetTransformations();
void repaint();
void onResize(const sf::Event &event);

int main(int argc, char *argv[])
{
    const char CONFIG_FILE[] = "cg6.conf";
    const char WINDOW_TITLE[] = "CG-6";

    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        std::cout << "Usage:" << std::endl
                  << argv[0] << "             to run visual application;" << std::endl
                  << argv[0] << " --help      to show this message;" << std::endl
                  << "See configuration file: " << CONFIG_FILE << std::endl;
    } else {
        try {
            Configuration configuration(CONFIG_FILE);
            window_width = configuration.getInt("window.width");
            window_height = configuration.getInt("window.height");
            const int window_vsync = configuration.getBool("window.vsync");
            const std::string texture_filename = configuration.getString("draw.texture");
            farest_plane = configuration.getFloat("plane.farest");
            nearest_plane = configuration.getFloat("plane.nearest");
            straight_step = configuration.getFloat("step.straight");
            angle_step = configuration.getFloat("step.angle");
            quads_vertices = configuration.getFloats("shapes.quads");
            lines_vertices = configuration.getFloats("shapes.lines");
            disk_vertices = build_disk(
                    configuration.getFloat("shapes.disk.radius"),
                    configuration.getInt("shapes.disk.slices")
                );
            octahedron_vertices = configuration.getFloats("shapes.octahedron");
            culling_enabled = configuration.getBool("draw.cull");
            color = configuration.getFloats("draw.color");
            texture_enabled = configuration.getBool("textures.enabled");
            quads_textures = configuration.getFloats("textures.quads");
            disk_textures = build_disk_texture(
                    configuration.getFloat("shapes.disk.radius"),
                    configuration.getInt("shapes.disk.slices")
            );            octahedron_textures = configuration.getFloats("textures.octahedron");
            resetTransformations();
            drawing_shape = DRAWING_QUADS;

            glutInit(&argc, argv);
            sf::VideoMode videoMode(window_width, window_height);
            sf::ContextSettings settings;
            settings.depthBits = 24;
            settings.stencilBits = 8;
            sf::RenderWindow window(videoMode, WINDOW_TITLE, sf::Style::Default, settings);
            window.setVerticalSyncEnabled(window_vsync);
            sf::Image texture_image;
            texture_image.loadFromFile(texture_filename);
            glGenTextures(1, &texture_handle);
            glBindTexture(GL_TEXTURE_2D, texture_handle);
            glTexImage2D(
                    GL_TEXTURE_2D, 0, GL_RGBA,
                    texture_image.getSize().x, texture_image.getSize().y,
                    0,
                    GL_RGBA, GL_UNSIGNED_BYTE, texture_image.getPixelsPtr()
            );
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glDepthFunc(GL_LESS);
            glClearDepth(1.0f);
            glDepthMask(GL_TRUE);
            glFrontFace(GL_CW);
            glCullFace(GL_BACK);
            glEnable(GL_DEPTH_TEST);
            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);

            bool running = true;
            while (running) {
                sf::Event event;
                while (window.pollEvent(event)) {
                    glMatrixMode(GL_MODELVIEW);
                    if (event.type == sf::Event::Closed) {
                        running = false;
                    } else if (event.type == sf::Event::MouseMoved) {
                        rotate_x = 360.0f * ((float) event.mouseMove.y / window_height) - 180.0f;
                        rotate_y = 360.0f * ((float) event.mouseMove.x / window_width) - 180.0f;
                    } else if (event.type == sf::Event::MouseWheelScrolled) {
                        if (event.mouseWheelScroll.delta < 0) {
                            scale_z += straight_step;
                        } else {
                            scale_z -= straight_step;
                        }
                    } else if (event.type == sf::Event::KeyPressed) {
                        onKeyPress(event);
                    } else if (event.type == sf::Event::Resized) {
                        onResize(event);
                    }
                }
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
                repaint();
                window.display();
            }
        } catch (std::runtime_error const& exc) {
            std::cerr << exc.what() << std::endl;
        }
    }

    return 0;
}

void onResize(const sf::Event &event)
{
    window_width = event.size.width;
    window_height = event.size.height;
    const float aspect = (float) event.size.width / (float) event.size.height;
    const float ymax = nearest_plane * tan(atan(1.0) / 2.0);
    const float ymin = -ymax;
    const float xmin = ymin * aspect;
    const float xmax = ymax * aspect;
    glViewport(0, 0, event.size.width, event.size.height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(xmin, xmax, ymin, ymax, nearest_plane, farest_plane);
}

void onKeyPress(const sf::Event &event)
{
    switch (event.key.code) {
        case sf::Keyboard::Comma:
            drawing_shape = (drawing_shape ? drawing_shape : DRAWING_MOD) - 1;
            break;
        case sf::Keyboard::Period:
            drawing_shape = (drawing_shape + 1) % DRAWING_MOD;
            break;
        case sf::Keyboard::W:
            translate_y += straight_step;
            break;
        case sf::Keyboard::S:
            translate_y -= straight_step;
            break;
        case sf::Keyboard::A:
            translate_x -= straight_step;
            break;
        case sf::Keyboard::D:
            translate_x += straight_step;
            break;
        case sf::Keyboard::Q:
            translate_z -= straight_step;
            break;
        case sf::Keyboard::Z:
            translate_z += straight_step;
            break;
        case sf::Keyboard::I:
            scale_y += straight_step;
            break;
        case sf::Keyboard::K:
            scale_y -= straight_step;
            break;
        case sf::Keyboard::J:
            scale_x -= straight_step;
            break;
        case sf::Keyboard::L:
            scale_x += straight_step;
            break;
        case sf::Keyboard::X:
            rotate_z += angle_step;
            break;
        case sf::Keyboard::C:
            rotate_z -= angle_step;
            break;
        case sf::Keyboard::V:
            texture_enabled = !texture_enabled;
            break;
        case sf::Keyboard::N:
            culling_enabled = !culling_enabled;
            break;
        case sf::Keyboard::T:
            color[0] = fmod(color[0] + 0.1f, 1.0f);
            break;
        case sf::Keyboard::G:
            color[1] = fmod(color[1] + 0.1f, 1.0f);
            break;
        case sf::Keyboard::B:
            color[2] = fmod(color[2] + 0.1f, 1.0f);
            break;
        case sf::Keyboard::Space:
            resetTransformations();
            break;
    }
}

std::vector<float> build_disk(float radius, int slices)
{
    const double segment = 8 * atan(1.0) / slices;
    std::vector<float> result;
    for (int i = 0; i < slices; i++) {
        result.push_back(cos(i * segment));
        result.push_back(sin(i * segment));
    }
    return result;
}

std::vector<float> build_disk_texture(float radius, int slices)
{
    const double segment = 8 * atan(1.0) / slices;
    std::vector<float> result;
    for (int i = 0; i < slices; i++) {
        result.push_back(0.75 + 0.25 * cos(i * segment));
        result.push_back(0.75 + 0.25 * sin(i * segment));
    }
    return result;
}

void resetTransformations()
{
    translate_x = 0;
    translate_y = 0;
    translate_z = -5;
    scale_x = 1;
    scale_y = 1;
    scale_z = 1;
    rotate_z = 0;
}

void repaint()
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glColor3fv(color.data());
    glScalef(scale_x, scale_y, scale_z);
    glTranslatef(translate_x, translate_y, translate_z);
    glRotatef(rotate_x, 1.0, 0.0, 0.0);
    glRotatef(rotate_y, 0.0, 1.0, 0.0);
    glRotatef(rotate_z, 0.0, 0.0, 1.0);
    switch (drawing_shape) {
        case DRAWING_QUADS:
            glVertexPointer(2, GL_FLOAT, 0, quads_vertices.data());
            if (texture_enabled) {
                glEnable(GL_TEXTURE_2D);
                glTexCoordPointer(2, GL_FLOAT, 0, quads_textures.data());
            }
            glDrawArrays(GL_QUADS, 0, quads_vertices.size() / 2);
            break;
        case DRAWING_LINES:
            glVertexPointer(2, GL_FLOAT, 0, lines_vertices.data());
            glDrawArrays(GL_LINES, 0, lines_vertices.size() / 2);
            break;
        case DRAWING_DISK:
            if (texture_enabled) {
                glEnable(GL_TEXTURE_2D);
                glTexCoordPointer(2, GL_FLOAT, 0, disk_textures.data());
            }
            glVertexPointer(2, GL_FLOAT, 0, disk_vertices.data());
            glDrawArrays(GL_POLYGON, 0, disk_vertices.size() / 2);
            break;
        case DRAWING_OCTAHEDRON:
            if (culling_enabled) {
                glEnable(GL_CULL_FACE);
            }
            glVertexPointer(3, GL_FLOAT, 0, octahedron_vertices.data());
            if (texture_enabled) {
                glEnable(GL_TEXTURE_2D);
                glTexCoordPointer(2, GL_FLOAT, 0, octahedron_textures.data());
                glDrawArrays(GL_TRIANGLES, 0, octahedron_vertices.size() / 3);
            } else {
                glDrawArrays(GL_LINE_LOOP, 0, octahedron_vertices.size() / 3);
            }
            glDisable(GL_CULL_FACE);
            break;
    }
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glColor3f(1.0, 1.0, 1.0);
    glRasterPos2f(-0.99f, +0.95f);
    glutBitmapString(GLUT_BITMAP_HELVETICA_12, (const unsigned char*) CONTROL_TIP);
    glPopMatrix();
}
