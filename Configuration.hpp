#pragma once

#include <map>
#include <string>
#include <vector>

class Configuration final
{

public:
    typedef std::map<std::string, int> named_const_t;

    Configuration(std::string const &filename);

    bool getBool(std::string const &key) const;
    float getFloat(std::string const &key) const;
    std::vector<float> getFloats(std::string const &key) const;
    int getInt(std::string const &key) const;
    std::string getString(std::string const &key) const;

private:
    std::map<std::string, std::string> values;

};
