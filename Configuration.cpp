#include "Configuration.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

Configuration::Configuration(std::string const &filename)
{
    std::ifstream input(filename);
    std::string line_string;
    while (std::getline(input, line_string)) {
        if (line_string.substr(0, 1) != "#" && line_string.size()) {
            std::istringstream line(line_string);
            std::string key, value;
            std::getline(line, key, '=');
            std::getline(line, value);
            values[key] = value;
        }
    }
}

bool Configuration::getBool(std::string const &key) const
{
    try {
        return values.at(key) == "true";
    } catch (std::out_of_range const& e) {
        throw std::runtime_error("No <" + key + "> key in configuration file!");
    }
}

float Configuration::getFloat(std::string const &key) const
{
    try {
        return std::stof(values.at(key));
    } catch (std::out_of_range const& e) {
        throw std::runtime_error("No <" + key + "> key in configuration file!");
    }
}

std::vector<float> Configuration::getFloats(std::string const &key) const
{
    try {
        std::vector<float> result;
        std::istringstream serialization(values.at(key));
        std::string value;
        while (std::getline(serialization, value, ';')) {
            result.push_back(std::stof(value));
        }
        return result;
    } catch (std::out_of_range const& e) {
        throw std::runtime_error("No <" + key + "> key in configuration file!");
    }
}

int Configuration::getInt(std::string const &key) const
{
    try {
        return std::stoi(values.at(key));
    } catch (std::out_of_range const& e) {
        throw std::runtime_error("No <" + key + "> key in configuration file!");
    }
}

std::string Configuration::getString(std::string const &key) const
{
    try {
        return values.at(key);
    } catch (std::out_of_range const& e) {
        throw std::runtime_error("No <" + key + "> key in configuration file!");
    }
}
